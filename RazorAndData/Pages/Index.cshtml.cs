﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using RazorAndData.Models;
using RazorAndData.Services;

namespace RazorAndData.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        public JsonFileProductService ProductSrevice;
        public IEnumerable<Product> Products;
        public JsonFilePhotoService PhotoService;
        public IEnumerable<Photo> Photos;
        public IndexModel(ILogger<IndexModel> logger, JsonFileProductService _productService ,JsonFilePhotoService _photoService)
        {
            _logger = logger;
            ProductSrevice = _productService;
            PhotoService = _photoService;
        }

        public void OnGet()
        {
            Products = ProductSrevice.GetProducts();
            Photos = PhotoService.GetPhotos();
        }
    }
}
